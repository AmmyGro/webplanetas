eg<%-- 
    Document   : Ver
    Created on : 8/06/2020, 06:05:08 PM
    Author     : PROBOOK 6470B
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
    import ="java.sql.Connection"        
    import ="java.sql.DriverManager"        
    import ="java.sql.ResultSet"        
    import ="java.sql.Statement"        
    import ="java.sql.SQLException"             
    %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ver</title>
        <link rel="stylesheet"href="Ver.css">
    </head>
    <body background="nebula.jpg">
         <h1><strong><font color="white">Cosmos Gromy</font></strong></h1>
        <table border="1">
            <p><strong>Mis Planetas</strong></p>

            <tr>
                <th>Posicion</th>
                <th>Nombre del planeta</th>
                <th>Informacion del planeta</th>
                <th>Fecha de creación</th>
                
               
            </tr>


            <%
                Connection conex = null;
                Statement sql = null;
                try {
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    conex = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/diarywebapp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "2203");
                    sql = conex.createStatement();

                    String qry = "select * from PlanetasWeb.registros";
                    ResultSet data = sql.executeQuery(qry);
                        while (data.next()) {
            %>

            <% if(data.getInt("Visibilidad") == 1){ %>
            <tr>
                <td>
                    <% out.print(data.getInt("Id"));%>
                </td>
                <td>
                    <% if(data.getInt("Planeta_Id")==1){%>
                            Mercurio
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==2){%>
                            Venus
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==3){%>
                            Tierra
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==4){%>
                            Marte
                            <% } %>
                        
                    <% if(data.getInt("Planeta_Id")==5){%>
                            Jupiter
                            <% } %>
                            
                    <% if(data.getInt("Planeta_Id")==6){%>
                            Saturno
                            <% } %>
                            
                    <% if(data.getInt("Planeta_Id")==7){%>
                            Urano
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==8){%>
                            Neptuno
                            <% } %>
                 
               </td> 
                <td>
                   <% out.print(data.getString("Informacion del Planeta"));%> 

                </td>
                <td>
                    <% out.print(data.getString("Fecha de creacion"));%>
                </td>
                
            </tr>

             <%  } %>

            <%
                    }
                    data.close();

                } catch (Exception e) {
                    out.print("Error en la conexión. Intenta de nuevo!");
                    e.printStackTrace();
                }

            %>
        </table>

        <button onclick="window.location.href = 'index.html';">Regresar al menú principal</button>

    </body>
</html>
